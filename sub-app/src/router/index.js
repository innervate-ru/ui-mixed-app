import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'

import mixedApp from '../../../mixed-app-manager/src'

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  const _push = Router.push;
  const _replace = Router.replace;
  const _go = Router.go;

  mixedApp.setHook({
    pushUrl: (url) => { _push.call(Router, url); },
    replaceUrl: (url) => { _replace.call(Router, url); },
    historyGo: (n) => { _go.call(Router, n); },
  });

  if (!mixedApp.isMain) {
    Router.push = (url) => mixedApp.pushUrl(typeof url === 'object' ? url.path : url);
    Router.replace = (url) => mixedApp.replaceUrl(typeof url === 'object' ? url.path : url);
    Router.go = (n) => mixedApp.historyGo(n);
  }

  return Router
}
