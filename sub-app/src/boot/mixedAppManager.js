import mixedApp from '../../../mixed-app-manager/src'

export default async ({Vue, app}) => {
  await mixedApp.init({
    storage: 'localStorage',
    toParentUrl: require('../../../main-app/src/mixedApp/subApp/toParentUrl').default,
  });
  await mixedApp.started;
}
