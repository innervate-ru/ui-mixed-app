import Vue from 'vue'
import ALink from '../components/ALink'

export default async ({ /* app, router, Vue, ... */ }) => {
  Vue.component('ALink', ALink);
}
