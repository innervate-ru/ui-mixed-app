export default {
  files: ['src/**/*.test.js'],
  concurrency: 5,
  failFast: true,
  failWithoutAssertions: false,
  tap: true,
  powerAssert: false,
  require: ["@babel/polyfill", "@babel/register"],
  babel: {
    testOptions: {
      babelrc: false,
      configFile: false,
      presets: ["@ava/babel-preset-stage-4"],
//      plugins: ["@babel/plugin-proposal-class-properties"]
    }
  }
};
