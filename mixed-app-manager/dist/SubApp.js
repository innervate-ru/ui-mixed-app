"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _arguments = require("./_arguments");

var _states = require("./_states");

var _MainApp2 = _interopRequireDefault(require("./MainApp"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { return function () { var Super = _getPrototypeOf(Derived), result; if (_isNativeReflectConstruct()) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var debug = require('debug')('mam'); // zork: Полный путь до debug, убирает ошибку в IE 11


var SubApp = /*#__PURE__*/function (_MainApp) {
  _inherits(SubApp, _MainApp);

  var _super = _createSuper(SubApp);

  function SubApp(options) {
    _classCallCheck(this, SubApp);

    return _super.call(this, options);
  }

  _createClass(SubApp, [{
    key: "pushUrl",
    value: function pushUrl(url) {
      if (!(typeof url === 'string')) (0, _arguments.invalidArgument)('url', url);
      debug("%s: pushUrl(%s)", this._name, url);
      if (this._state !== _states.READY) throw new Error("Invalid state: ".concat(this._state));

      if (this._isMain) {
        _get(_getPrototypeOf(SubApp.prototype), "_pushUrl", this).call(this, url);
      } else {
        this.sendToParent({
          type: 'push-url',
          url: url
        });
      }
    }
  }, {
    key: "replaceUrl",
    value: function replaceUrl(url) {
      if (!(typeof url === 'string')) (0, _arguments.invalidArgument)('url', url);
      debug("%s: replaceUrl(%s)", this._name, url);
      if (this._state !== _states.READY) throw new Error("Invalid state: ".concat(this._state));

      if (this._isMain) {
        _get(_getPrototypeOf(SubApp.prototype), "_replaceUrl", this).call(this, url);
      } else {
        this.sendToParent({
          type: 'replace-state',
          url: url
        });
      }
    }
  }, {
    key: "historyGo",
    value: function historyGo(n) {
      if (!(typeof n !== 'number' && n > 0)) (0, _arguments.invalidArgument)('n', n);
      debug("%s: historyGo(%d)", this._name, n);
      if (this._state !== _states.READY) throw new Error("Invalid state: ".concat(this._state));

      if (this._isMain) {
        _get(_getPrototypeOf(SubApp.prototype), "_historyGo", this).call(this, url);
      } else {
        this.sendToParent({
          type: 'history-go',
          n: n
        });
      }
    }
  }, {
    key: "newTab",
    value: function newTab(url) {
      if (!(typeof url === 'string')) (0, _arguments.invalidArgument)('url', url);
      debug("%s: newTab(%s)", this._name, url);
      if (this._state !== _states.READY) throw new Error("Invalid state: ".concat(this._state));

      if (this._isMain) {
        _get(_getPrototypeOf(SubApp.prototype), "_newTab", this).call(this, url);
      } else {
        this.sendToParent({
          type: 'new-tab',
          url: url
        });
      }
    }
  }, {
    key: "sendToParent",
    value: function sendToParent(msg) {
      debug("%s: _sendToParent(%o)", this._name, msg);
      window.parent.postMessage({
        source: 'mixed-app-manager',
        payload: msg
      }, '*');
    }
  }, {
    key: "_processMessage",
    value: function _processMessage(event) {
      if (!(_typeof(event) === 'object' && event !== null && event.data.source === 'mixed-app-manager')) return;
      debug("%s: _processMessage(%o)", this._name, event);
      var payload = event.data.payload;

      if (payload.type === 'broadcast') {
        for (var i = this._listeners.length; i-- > 0;) {
          // в обратной последовательности, чтоб listener мог себя безопастно в процессе удалить
          var listener = this._listeners[i];
          listener(payload.msg);
        }

        this.sendToAllLoaded(payload.msg);
      } else if (this._isMain) {
        _get(_getPrototypeOf(SubApp.prototype), "_processMessage", this).call(this, event);
      } else {
        switch (payload.type) {
          case 'storage':
            {
              this._storage._update(payload.value);

              if (this._state !== _states.READY) {
                this._appReadyResolve();

                delete this._appReadyResolve;
                this.sendToParent({
                  type: 'ready'
                });
              }

              break;
            }

          case 'push-url':
            {
              _get(_getPrototypeOf(SubApp.prototype), "_pushUrl", this).call(this, payload.url);

              break;
            }

          case 'replace-url':
            {
              _get(_getPrototypeOf(SubApp.prototype), "_replaceUrl", this).call(this, payload.url);

              break;
            }

          case 'history-go':
            {
              _get(_getPrototypeOf(SubApp.prototype), "_historyGo", this).call(this, payload.n);

              break;
            }

          default:
            throw new Error("Unexpected message: ".concat(JSON.stringify(payload)));
        }
      }
    }
  }]);

  return SubApp;
}(_MainApp2["default"]);

var _default = SubApp;
exports["default"] = _default;
