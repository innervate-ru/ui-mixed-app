"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.childUrl = exports.name = exports.isMain = exports.parsedURL = void 0;

var _url = _interopRequireDefault(require("url"));

var _queryString = _interopRequireDefault(require("query-string"));

var _SubApp = _interopRequireDefault(require("./SubApp"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var debug = require('debug')('mam'); // zork: Полный путь до debug, убирает ошибку в IE 11


var parsedURL = _url["default"].parse(window.location.href);

exports.parsedURL = parsedURL;

var query = _queryString["default"].parse(parsedURL.query);

var isMain = !query._ma;
exports.isMain = isMain;
var name = isMain ? 'main' : decodeURI(query._ma);
exports.name = name;
var childUrl = query._maChildUrl;
exports.childUrl = childUrl;
var parentHost = query._maParentHost;
debug('%s: started (url: %s)', name, window.location.href);

if (!isMain) {
  // удаляем параметр _ma из URL, чтоб он не попал при переходах в верхнее приложение
  delete query._ma;
  delete query._maParentHost;
  delete query._maChildUrl;
  parsedURL.search = _queryString["default"].stringify(query);
  window.history.replaceState(undefined, '', _url["default"].format(parsedURL));
}

var app = new _SubApp["default"]({
  isMain: isMain,
  name: name,
  parentHost: parentHost
});
window.addEventListener('message', app._processMessage.bind(app));

if (!app.isMain) {
  debug("%s: state: loaded", app.name);
  app.sendToParent({
    type: 'loaded'
  });
}

var _default = app;
exports["default"] = _default;
