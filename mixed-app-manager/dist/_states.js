"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.READY = exports.LOADED = exports.LOADING = exports.INITIALIZING = exports.INIT = void 0;
var INIT = 'init'; // initial

exports.INIT = INIT;
var INITIALIZING = 'initializing'; // initialization step is completed; MainAppSubApp not using this step

exports.INITIALIZING = INITIALIZING;
var LOADING = 'loading'; // MainAppSubApp - had started loading iframe; other types of apps skip it

exports.LOADING = LOADING;
var LOADED = 'loaded'; // MainAppSubApp - iframe is loaded; other types of apps skip it

exports.LOADED = LOADED;
var READY = 'ready'; // app is loaded and storage is initialized.  app is ready to work

exports.READY = READY;