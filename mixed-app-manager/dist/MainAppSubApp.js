"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _url = _interopRequireDefault(require("url"));

var _arguments = require("./_arguments");

var _states = require("./_states");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var debug = require('debug')('mam'); // zork: Полный путь до debug, убирает ошибку в IE 11


var MainAppSubApp = /*#__PURE__*/function () {
  /**
   * Добавляет приложение.
   *
   * Приложение описывается структурой:
   * { name - имя приложения, url - адрес по которому загружается приложение, приложение загружается сразу}
   *
   * Чтобы добавить несколько приложений, надо передать несколько аргументов.
   */
  function MainAppSubApp(mainApp, args) {
    _classCallCheck(this, MainAppSubApp);

    if (!(typeof mainApp.addSubApp === 'function')) (0, _arguments.invalidArgument)('mainApp', mainApp);
    if (!(_typeof(args) === 'object' && args != null)) (0, _arguments.invalidArgument)("args", args);
    if (!args.baseUrl) (0, _arguments.missingArgument)("baseUrl");
    Object.keys(args).forEach(function (k) {
      var t = args[k];

      switch (k) {
        case 'name':
        case 'baseUrl':
          if (!(typeof t === 'string' && t.length > 0)) (0, _arguments.invalidArgument)("".concat(k), t);
          break;

        case 'toParentUrl':
        case 'toChildUrl':
        case 'onAttach':
        case 'onDetach':
          if (!(t === undefined || typeof t === 'function')) (0, _arguments.invalidArgument)("".concat(k), t);
          break;

        default:
          (0, _arguments.unexpectedArgument)("".concat(k), t);
      }
    });
    this._mainApp = mainApp;
    this._shortName = args.name;
    this._name = "".concat(mainApp.name, "._").concat(args.name);
    this._state = _states.INIT;
    this._shortName = args.name;
    this._baseUrl = args.baseUrl;
    this._toParentUrl = args.toParentUrl === undefined ? identity : args.toParentUrl.bind(this);
    this._toChildUrl = args.toChildUrl === undefined ? identity : args.toChildUrl.bind(this);
    this._onAttach = args.onAttach || null;
    this._onDetach = args.onDetach || null;
    this._currentURL = null;

    var parsedBaseURL = _url["default"].parse(this._baseUrl);

    this._parentHost = parsedBaseURL.host ? "".concat(window.location.protocol, "//").concat(window.location.host) : null;
  }

  _createClass(MainAppSubApp, [{
    key: "pushUrl",
    value: function pushUrl(url) {
      if (!(typeof url === 'string')) (0, _arguments.invalidArgument)('url', url);
      debug("%s: pushUrl(%o)", this._name, url);

      if (url !== this._currentURL) {
        this._currentURL = url;

        if (this._state === _states.READY) {
          var childUrl = this._toChildUrl(url);

          if (childUrl) this.sendMessage({
            type: 'push-url',
            url: childUrl
          });
        } else {
          debug("%s: pushUrl postponed until ready state");
          this._pushOnReadyURL = url;
        }
      } else {
        debug("%s: pushUrl skip cause it's the same as url before");
      }
    }
  }, {
    key: "replaceUrl",
    value: function replaceUrl(url) {
      if (!(typeof url === 'string')) (0, _arguments.invalidArgument)('url', url);
      debug("%s: replaceUrl(%o)", this._name, url);

      if (url !== this._currentURL) {
        this._currentURL = url;

        if (this._state === _states.READY) {
          var childUrl = this._toChildUrl(url);

          if (childUrl) this.sendMessage({
            type: 'replace-url',
            url: childUrl
          });
        } else {
          this._pushOnReadyURL = url;
        }
      }
    }
  }, {
    key: "historyGo",
    value: function historyGo(n) {
      if (!(typeof n === 'number' && n > 0)) (0, _arguments.invalidArgument)('n', n);
      debug("%s: historyGo(%o)", this._name, n);
      if (this._state !== _states.READY) throw new Error("Invalid state: ".concat(this._state));
      this.sendMessage({
        type: 'history-go',
        n: n
      });
    }
  }, {
    key: "attach",

    /**
     * Возвращает iframe вложенного приложения.  Этот метод можно использовать только после вызова метода getApp.
     *
     * @param args.url - путь для открытия iframe.  Используется только при самом первом вызове приложения.
     * @returns {HTMLIFrameElement}
     */
    value: function attach(div, args) {
      var _this = this;

      debug("%s: attach(%o)", this._name, args);
      if (this._iframe) this.detach();
      debug("%s: create new iframe", this._name);
      var url, width, height;

      if (args) {
        url = args.url;
        height = args.height;
        width = args.width;
        if (!(url === undefined || typeof url === 'string')) (0, _arguments.invalidArgument)("url", url);
        if (!(width === undefined || typeof width === 'string')) (0, _arguments.invalidArgument)("width", width);
        if (!(height === undefined || typeof height === 'string')) (0, _arguments.invalidArgument)("height", height);
      }

      if (!url) {
        var loc = window.location;
        url = "".concat(loc.pathname).concat(loc.search).concat(loc.hash); // zork: По умолчанию используем текущий путь из браузера
      }

      this._currentURL = url;
      url = this._toChildUrl(url);

      var parsedURL = _url["default"].parse("".concat(this._baseUrl).concat(url));

      if (parsedURL.search) {
        parsedURL.search += "&_ma=".concat(this._mainApp._name, ".").concat(this._shortName).concat(this._parentHost ? "&_maParentHost=".concat(encodeURI(this._parentHost)) : '', "&_maChildUrl=").concat(encodeURI(url));
      } else {
        parsedURL.search = "?_ma=".concat(this._mainApp._name, ".").concat(this._shortName).concat(this._parentHost ? "&_maParentHost=".concat(encodeURI(this._parentHost)) : '', "&_maChildUrl=").concat(encodeURI(url));
      }

      this._state = _states.LOADING;
      this._iframe = document.createElement('iframe');

      this._iframe.setAttribute('frameBorder', '0');

      this._iframe.setAttribute('src', _url["default"].format(parsedURL));

      if (width) this._iframe.setAttribute('width', width);
      if (height) this._iframe.setAttribute('height', height);
      this._div = div;
      div.appendChild(this._iframe);
      this._appReadyPromise = new Promise(function (resolve, reject) {
        _this._appReadyResolve = function () {
          debug("%s: state: ready", _this._name);
          _this._state = _states.READY;
          delete _this._appReadyResolve;
          delete _this._appReadyReject;
          resolve(_this);
        };

        _this._appReadyReject = function () {
          debug("%s: attach cancelled", _this._name);
          _this._state = _states.INIT;
          delete _this._appReadyResolve;
          delete _this._appReadyReject;
          reject();
        };
      });
      if (this._onAttach) this._onAttach();
      return this._appReadyPromise;
    }
  }, {
    key: "getIFrame",
    value: function getIFrame() {
      return this._iframe;
    }
  }, {
    key: "detach",
    value: function detach() {
      if (this._state !== _states.INIT) {
        if (this._state !== _states.READY) {
          delete this._appReadyReject();
        }

        if (this._iframe) {
          this._div.removeChild(this._iframe);

          delete this._iframe;
          delete this._div;
        }

        this._state = _states.INIT;
        if (this._onDetach) this._onDetach();
      }
    }
    /**
     * Обрабатывает payload сообщений полученных из соотвествующего iframe и признаком что это сообщение mixed-app-manager'а.
     *
     * @param payload
     * @private
     */

  }, {
    key: "_processMessage",
    value: function _processMessage(payload) {
      debug("%s: _processMessage(%o)", this._name, payload);

      switch (payload.type) {
        case 'loaded':
          {
            this._state = _states.LOADED;

            this._mainApp._storage._sendStateToApp(this);

            break;
          }

        case 'ready':
          {
            if (this._pushOnReadyURL) {
              var url = this._toChildUrl(this._pushOnReadyURL);

              if (url) this.sendMessage({
                type: 'push-url',
                url: url
              });
              delete this._pushOnReadyURL;
            }

            this._appReadyResolve();

            break;
          }

        case 'push-url':
          {
            this._mainApp.pushUrl(this._toParentUrl(payload.url));

            break;
          }

        case 'replace-state':
          {
            this._mainApp.replaceUrl(this._toParentUrl(payload.url));

            break;
          }

        case 'history-go':
          {
            this._mainApp.historyGo(payload.n);

            break;
          }

        case 'new-tab':
          {
            this._mainApp.newTab(payload.url);

            break;
          }

        case 'storage':
          {
            this._mainApp._storage.update(payload.value);

            break;
          }

        default:
          throw new Error("Unexpected message: ".concat(JSON.stringify(payload)));
      }
    }
  }, {
    key: "sendMessage",

    /**
     * Отправляет сообщение в окно вложенного приложения.
     *
     * @param msg - сообщение
     */
    value: function sendMessage(msg) {
      debug("%s: sendMessage(%o)", this._name, msg);

      if (this._iframe) {
        this._iframe.contentWindow.postMessage({
          source: 'mixed-app-manager',
          payload: msg
        }, '*');
      } else {
        debug("%s: no iframe", this._name);
      }
    }
  }]);

  return MainAppSubApp;
}();

Object.defineProperties(MainAppSubApp.prototype, {
  name: {
    get: function get() {
      return this._name;
    }
  },
  started: {
    get: function get() {
      return this._appReadyPromise;
    }
  },
  _state: {
    set: function set(v) {
      debug("%s: _state = %s", this.name, v);
      this.__state = v;
    },
    get: function get() {
      return this.__state;
    }
  }
});

function identity(v) {
  return v;
}

var _default = MainAppSubApp;
exports["default"] = _default;
