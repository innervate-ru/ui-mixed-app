"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.missingArgument = missingArgument;
exports.invalidArgument = invalidArgument;
exports.unexpectedArgument = unexpectedArgument;

function missingArgument(name) {
  throw new Error("Missing argument '".concat(name, "'"));
}

function invalidArgument(name, value) {
  throw new Error("Invalid argument '".concat(name, "' : ").concat(JSON.stringify(value)));
}

function unexpectedArgument(name, value) {
  throw new Error("Unexpected argument '".concat(name, "' : ").concat(JSON.stringify(value)));
}