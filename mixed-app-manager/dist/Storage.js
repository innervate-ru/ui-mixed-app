"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _arguments = require("./_arguments");

var _states = require("./_states");

var _MainApp = _interopRequireDefault(require("./MainApp"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(n); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var debug = require('debug')('mam'); // zork: Полный путь до debug, убирает ошибку в IE 11


var Storage = /*#__PURE__*/function () {
  function Storage(_ref) {
    var _this = this;

    var app = _ref.app,
        doSafeToStorage = _ref.doSafeToStorage,
        storage = _ref.storage;

    _classCallCheck(this, Storage);

    this._state = Object.create(null);
    this._listeners = [];
    this._app = app;
    this._storage = app.isMain && storage ? window[storage] : null;
    this._doSafeToStorage = doSafeToStorage;

    if (this._storage) {
      Object.entries(this._storage).forEach(function (_ref2) {
        var _ref3 = _slicedToArray(_ref2, 2),
            key = _ref3[0],
            value = _ref3[1];

        return _this._state[key] = value;
      });
    }
  }

  _createClass(Storage, [{
    key: "update",
    value: function update() {
      var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : (0, _arguments.missingArgument)('state');
      if (!(_typeof(state) === 'object' && state != null && Object.values(state).every(function (v) {
        return v === undefined || typeof v === 'string';
      }))) (0, _arguments.invalidArgument)('state', state);
      debug("%s: update(%o)", this._app._name, state);

      if (this._app._isMain) {
        this._update(state);
      } else {
        this._app.sendToParent({
          type: 'storage',
          value: state
        });
      }
    }
    /**
     * Обновления полученные через сообщение - только для распостранения обновлений сверху вниз.
     * @private
     */

  }, {
    key: "_update",
    value: function _update() {
      var _this2 = this;

      var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : (0, _arguments.missingArgument)('state');
      var prevState = this._state;
      var newState = this._state = Object.create(null);
      Object.entries(prevState).forEach(function (_ref4) {
        var _ref5 = _slicedToArray(_ref4, 2),
            key = _ref5[0],
            value = _ref5[1];

        newState[key] = value;
      });
      Object.entries(state).forEach(function (_ref6) {
        var _ref7 = _slicedToArray(_ref6, 2),
            key = _ref7[0],
            value = _ref7[1];

        if (value === undefined) {
          delete newState[key];
        } else {
          newState[key] = value;
        }
      });

      for (var i = this._listeners.length; i-- > 0;) {
        // в обратной последовательности, чтоб listener мог себя безопастно в процессе удалить
        var listener = this._listeners[i];
        listener(newState, prevState);
      }

      if (this._app.isMain) {
        if (this._storage) {
          var doSafeToStorage = this._doSafeToStorage;
          Object.entries(state).forEach(function (_ref8) {
            var _ref9 = _slicedToArray(_ref8, 2),
                key = _ref9[0],
                value = _ref9[1];

            if (doSafeToStorage && !doSafeToStorage(key)) return;

            if (value === undefined) {
              _this2._storage.removeItem(key);
            } else {
              _this2._storage.setItem(key, value);
            }
          });
        }

        this._app.sendToAllLoaded({
          type: 'storage',
          value: state
        });
      }
    }
  }, {
    key: "_sendStateToApp",
    value: function _sendStateToApp(subApp) {
      subApp.sendMessage({
        type: 'storage',
        value: this._state
      });
    }
  }, {
    key: "setItem",
    value: function setItem(key, value) {
      if (!(typeof key === 'string')) (0, _arguments.invalidArgument)('key', key); // if (!(typeof value === 'string')) invalidArgument('value', value);

      debug("%s: setItem(%s, %s)", this._app._name, key, value);
      this.update(_defineProperty({}, key, "".concat(value)));
    }
  }, {
    key: "getItem",
    value: function getItem(key) {
      if (!(typeof key === 'string')) (0, _arguments.invalidArgument)('key', key);
      debug("%s: getItem(%s)", this._app._name, key);
      return this._state[key] || null;
    }
    /**
     * @param listener
     * @returns {Function} - метод, чтоб отписаться от получения обновлений состояния
     */

  }, {
    key: "onUpdate",
    value: function onUpdate(listener, noTimeout) {
      var _this3 = this;

      if (!(typeof listener === 'function')) (0, _arguments.invalidArgument)('listener', listener);
      debug("%s: onUpdate()", this._app._name);

      this._listeners.push(listener);

      if (noTimeout) {
        // во app.vue created() нужно чтобы listener сработал синронно сразу - иначе срабатыват логика до момента когда удается получить данные о пользователе
        listener(this._state, this._state);
      } else {
        setTimeout(function () {
          return listener(_this3._state, _this3._state);
        }, 0);
      }

      return function () {
        var index = _this3._listeners.findIndex(function (v) {
          return v === listener;
        });

        if (index >= 0) {
          _this3._listeners.splice(index, 1);
        }
      };
    }
  }]);

  return Storage;
}();

Object.defineProperties(Storage.prototype, {
  state: {
    get: function get() {
      return this._state;
    }
  }
});
var _default = Storage;
exports["default"] = _default;
