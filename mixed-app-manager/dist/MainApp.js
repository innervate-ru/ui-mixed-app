"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _url2 = _interopRequireDefault(require("url"));

var _queryString = _interopRequireDefault(require("query-string"));

var _arguments = require("./_arguments");

var _MainAppSubApp = _interopRequireDefault(require("./MainAppSubApp"));

var _Storage = _interopRequireDefault(require("./Storage"));

var _states = require("./_states");

var _index = require("./index");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(n); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var debug = require('debug')('mam'); // zork: Полный путь до debug, убирает ошибку в IE 11


var MainApp = /*#__PURE__*/function () {
  function MainApp(_ref) {
    var _this = this;

    var isMain = _ref.isMain,
        name = _ref.name,
        parentHost = _ref.parentHost;

    _classCallCheck(this, MainApp);

    this._subApps = Object.create(null);
    this._state = _states.INIT;
    this._isMain = isMain;
    this._name = name;
    this._storage = null;
    this._appReadyPromise = new Promise(function (resolve, reject) {
      _this._appReadyResolve = function () {
        debug("%s: state: ready", _this._name);
        _this._state = _states.READY;
        delete _this._appReadyResolve;
        resolve(_this);
      };
    });
    this._parentHost = parentHost;
    this._listeners = [];
  }

  _createClass(MainApp, [{
    key: "init",
    value: function init(args) {
      // zork: Почему то транспайлер глючит, и оставляет этот async в ES5 коде
      debug("%s: init(%o)", this._name, args);
      var storage, subApps, doSafeToStorage, toParentUrl;

      if (args !== undefined) {
        if (!(_typeof(args) === 'object' && args != null)) (0, _arguments.invalidArgument)('args', args);
        Object.entries(args).forEach(function (_ref2) {
          var _ref3 = _slicedToArray(_ref2, 2),
              key = _ref3[0],
              value = _ref3[1];

          switch (key) {
            case 'storage':
              {
                if (!(value === undefined || value === 'localStorage' || value === 'sessionStorage')) (0, _arguments.invalidArgument)('storage', value);
                storage = value;
                break;
              }

            case 'subApps':
              {
                if (!(!value || Array.isArray(value) && value.every(function (v) {
                  return _typeof(v) === 'object' && v !== null;
                }))) (0, _arguments.invalidArgument)('subApps', value);
                subApps = value;
                break;
              }

            case 'doSafeToStorage':
              {
                if (!(!value || typeof value === 'function')) (0, _arguments.invalidArgument)('doSafeToStorage', value);
                doSafeToStorage = value;
                break;
              }

            case 'toParentUrl':
              {
                if (!(!value || typeof value === 'function')) (0, _arguments.invalidArgument)('toParentUrl', value);
                toParentUrl = value;
                break;
              }

            default:
              (0, _arguments.unexpectedArgument)(key, value);
          }
        });
      }

      this._storage = new _Storage["default"]({
        app: this,
        doSafeToStorage: doSafeToStorage,
        storage: storage
      });
      this._toParentUrl = _index.isMain || !toParentUrl ? function (url) {
        return url;
      } : toParentUrl.bind(this);
      /* zork: Это оказалось лишней логикой
          if (this._parentHost) {
            const prevToParentUrl = this._toParentUrl;
            const {protocol, hostname, port} = URL.parse(this._parentHost);
            this._toParentUrl = function (url) {
              return URL.format({
                ...URL.parse(prevToParentUrl(url)),
                protocol,
                hostname,
                port,
              });
            }
          }
      */

      if (subApps) {
        this.addSubApp.apply(this, subApps);
      }

      if (this._isMain) {
        this._appReadyResolve();
      }
    }
  }, {
    key: "_pushUrl",
    value: function _pushUrl(url) {
      debug("%s: _pushUrl(%s); this._hook.pushUrl: %o", this._name, url, this._hook && this._hook.pushUrl);
      this._hook && this._hook.pushUrl ? this._hook.pushUrl(url) : window.history.pushState(null, '', url);
    }
  }, {
    key: "_replaceUrl",
    value: function _replaceUrl(url) {
      debug("%s: _replaceUrl(%s)", this._name, url);
      this._hook && this._hook.replaceUrl ? this._hook.replaceUrl(url) : window.history.replaceState(null, '', url);
    }
  }, {
    key: "_historyGo",
    value: function _historyGo(n) {
      this._hook && this._hook.historyGo ? this._hook.historyGo(n) : window.history.go(n);
    }
  }, {
    key: "_newTab",
    value: function _newTab(url) {
      this._hook && this._hook.newTab ? this._hook.newTab(url) : window.open(url, '_blank');
    }
  }, {
    key: "setHook",
    value: function setHook(hook) {
      this._hook = hook;
    }
  }, {
    key: "_url",
    value: function _url(url) {
      if (_typeof(url) === 'object' && url !== null) {
        if (url.query) {
          var parsedUrl = _url2["default"].parse(url.path);

          _index.parsedURL.search = "?".concat(_queryString["default"].stringify(url.query));

          var newUrl = _objectSpread({}, parsedUrl, {
            search: _queryString["default"].stringify(url.query)
          });

          return _url2["default"].format(newUrl);
        }

        return url.path;
      }

      return url;
    }
  }, {
    key: "toParentUrl",
    value: function toParentUrl(url) {
      return this._toParentUrl(this._url(url));
    }
    /**
     * Добавляет приложение.
     *
     * Приложение описывается структурой:
     * {
     * name - имя приложения,
     * baseUrl - базовый адрес приложения, без слеша на конце (например: http://localhost:8090),
     * blankPage? - пустая страница приложения (используется при переходах, чтоб не было видно предыдущего состояния)
     * toParentUrl? - функция преобразование адресов из вложенного приложения в главное приложение,
     * toParentUrl? - функция преобразование адресов из главного приложения во вложенное прилжение
     * onAttach? - вызывается в момент создания iframe
     * onDetach? - вызывается в момент удаления iframe
     * }
     *
     * Чтобы добавить несколько приложений, надо передать несколько аргументов.
     */

  }, {
    key: "addSubApp",
    value: function addSubApp() {
      var _this2 = this;

      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      debug("%s: addSubApp(%o)", this._name, args);
      args.forEach(function (v) {
        var subapp = new _MainAppSubApp["default"](_this2, v);
        if (Object.prototype.hasOwnProperty.call(_this2._subApps, subapp._shortName)) throw new Error("Duplicated app name: ".concat(subapp.name));
        _this2._subApps[subapp._shortName] = subapp;
      });
    }
  }, {
    key: "_processMessage",
    value: function _processMessage(event) {
      var payload = event.data.payload;
      /* zork: same logic in the SubApp class
          if (!(typeof event === 'object' && event !== null && event.data.source === 'mixed-app-manager')) return;
          debug(`%s: _processMessage(%o)`, this._name, event);
      */

      var subapp = Object.values(this._subApps).find(function (app) {
        return app._iframe && event.source === app._iframe.contentWindow;
      });

      if (!subapp) {
        throw new Error("Unexpected event.origin");
      }

      subapp._processMessage(event.data.payload);
    }
  }, {
    key: "getApp",

    /* async */
    value: function getApp(name, args) {
      debug("%s: getApp(%s, %o)", this._name, name, args);
      if (!this._subApps[name]) throw new Error("Missing app with name '".concat(name, "'"));
      return this._subApps[name];
    }
  }, {
    key: "sendToAllLoaded",
    value: function sendToAllLoaded(msg) {
      Object.values(this._subApps).forEach(function (app) {
        if (app._state !== _states.INIT) app.sendMessage(msg);
      });
    }
  }, {
    key: "broadcast",
    value: function broadcast(msg) {
      for (var i = this._listeners.length; i-- > 0;) {
        // в обратной последовательности, чтоб listener мог себя безопастно в процессе удалить
        var listener = this._listeners[i];
        listener(msg);
      }

      this.sendToAllLoaded({
        msg: msg,
        type: 'broadcast'
      });
    }
    /**
     * @param listener
     * @returns {Function} - метод, чтоб отписаться от получения обновлений состояния
     */

  }, {
    key: "onMessage",
    value: function onMessage(listener) {
      var _this3 = this;

      if (!(typeof listener === 'function')) (0, _arguments.invalidArgument)('listener', listener);
      debug("%s: onUpdate()", this._app._name);

      this._listeners.push(listener);

      return function () {
        var index = _this3._listeners.findIndex(function (v) {
          return v === listener;
        });

        if (index >= 0) {
          _this3._listeners.splice(index, 1);
        }
      };
    }
  }]);

  return MainApp;
}();

Object.defineProperties(MainApp.prototype, {
  isMain: {
    get: function get() {
      return this._isMain;
    }
  },
  name: {
    get: function get() {
      return this._name;
    }
  },
  storage: {
    get: function get() {
      return this._storage;
    }
  },
  started: {
    get: function get() {
      return this._appReadyPromise;
    }
  }
});
var _default = MainApp;
exports["default"] = _default;
