import URL from 'url'
import {invalidArgument, missingArgument, unexpectedArgument} from "./_arguments"
import {INIT, LOADING, LOADED, READY} from "./_states"

const debug = require('debug')('mam'); // zork: Полный путь до debug, убирает ошибку в IE 11

class MainAppSubApp {
  /**
   * Добавляет приложение.
   *
   * Приложение описывается структурой:
   * { name - имя приложения, url - адрес по которому загружается приложение, приложение загружается сразу}
   *
   * Чтобы добавить несколько приложений, надо передать несколько аргументов.
   */
  constructor(mainApp, args) {
    if (!(typeof mainApp.addSubApp === 'function')) invalidArgument('mainApp', mainApp);
    if (!(typeof args === 'object' && args != null)) invalidArgument(`args`, args);
    if (!args.baseUrl) missingArgument(`baseUrl`);
    Object.keys(args).forEach((k) => {
      const t = args[k];
      switch (k) {
        case 'name':
        case 'baseUrl':
          if (!(typeof t === 'string' && t.length > 0)) invalidArgument(`${k}`, t);
          break;
        case 'toParentUrl':
        case 'toChildUrl':
        case 'onAttach':
        case 'onDetach':
          if (!(t === undefined || typeof t === 'function')) invalidArgument(`${k}`, t);
          break;
        default:
          unexpectedArgument(`${k}`, t);
      }
    });
    this._mainApp = mainApp;
    this._shortName = args.name;
    this._name = `${mainApp.name}._${args.name}`
    this._state = INIT;
    this._shortName = args.name;
    this._baseUrl = args.baseUrl;
    this._toParentUrl = args.toParentUrl === undefined ? identity : args.toParentUrl.bind(this);
    this._toChildUrl = args.toChildUrl === undefined ? identity : args.toChildUrl.bind(this);
    this._onAttach = args.onAttach || null;
    this._onDetach = args.onDetach || null;
    this._currentURL = null;

    const parsedBaseURL = URL.parse(this._baseUrl);
    this._parentHost = parsedBaseURL.host ? `${window.location.protocol}//${window.location.host}` : null;
  }

  pushUrl(url) {
    if (!(typeof url === 'string')) invalidArgument('url', url);
    debug(`%s: pushUrl(%o)`, this._name, url);
    if (url !== this._currentURL) {
      this._currentURL = url;
      if (this._state === READY) {
        const childUrl = this._toChildUrl(url);
        if (childUrl) this.sendMessage({
          type: 'push-url',
          url: childUrl,
        });
      } else {
        debug(`%s: pushUrl postponed until ready state`);
        this._pushOnReadyURL = url;
      }
    } else {
      debug(`%s: pushUrl skip cause it's the same as url before`);
    }
  };

  replaceUrl(url) {
    if (!(typeof url === 'string')) invalidArgument('url', url);
    debug(`%s: replaceUrl(%o)`, this._name, url);
    if (url !== this._currentURL) {
      this._currentURL = url;
      if (this._state === READY) {
        const childUrl = this._toChildUrl(url);
        if (childUrl) this.sendMessage({
          type: 'replace-url',
          url: childUrl,
        });
      } else {
        this._pushOnReadyURL = url;
      }
    }
  };

  historyGo(n) {
    if (!(typeof n === 'number' && n > 0)) invalidArgument('n', n);
    debug(`%s: historyGo(%o)`, this._name, n);
    if (this._state !== READY) throw new Error(`Invalid state: ${this._state}`);
    this.sendMessage({
      type: 'history-go',
      n,
    });
  };

  /**
   * Возвращает iframe вложенного приложения.  Этот метод можно использовать только после вызова метода getApp.
   *
   * @param args.url - путь для открытия iframe.  Используется только при самом первом вызове приложения.
   * @returns {HTMLIFrameElement}
   */
  attach(div, args) {
    debug(`%s: attach(%o)`, this._name, args);
    if (this._iframe) this.detach();
    debug(`%s: create new iframe`, this._name);
    let url, width, height;
    if (args) {
      ({url, height, width} = args);
      if (!(url === undefined || typeof url === 'string')) invalidArgument(`url`, url);
      if (!(width === undefined || (typeof width === 'string'))) invalidArgument(`width`, width);
      if (!(height === undefined || (typeof height === 'string'))) invalidArgument(`height`, height);
    }
    if (!url) {
      const loc = window.location;
      url = `${loc.pathname}${loc.search}${loc.hash}` // zork: По умолчанию используем текущий путь из браузера
    }

    this._currentURL = url;

    url = this._toChildUrl(url);
    const parsedURL = URL.parse(`${this._baseUrl}${url}`);
    if (parsedURL.search) {
      parsedURL.search += `&_ma=${this._mainApp._name}.${this._shortName}${this._parentHost ? `&_maParentHost=${encodeURI(this._parentHost)}` : ''}&_maChildUrl=${encodeURI(url)}`;
    } else {
      parsedURL.search = `?_ma=${this._mainApp._name}.${this._shortName}${this._parentHost ? `&_maParentHost=${encodeURI(this._parentHost)}` : ''}&_maChildUrl=${encodeURI(url)}`;
    }

    this._state = LOADING;
    this._iframe = document.createElement('iframe');
    this._iframe.setAttribute('frameBorder', '0');
    this._iframe.setAttribute('src', URL.format(parsedURL));
    if (width) this._iframe.setAttribute('width', width);
    if (height) this._iframe.setAttribute('height', height);
    this._div = div;
    div.appendChild(this._iframe);

    this._appReadyPromise = new Promise((resolve, reject) => {
      this._appReadyResolve = () => {
        debug(`%s: state: ready`, this._name);
        this._state = READY;
        delete this._appReadyResolve;
        delete this._appReadyReject;
        resolve(this);
      };
      this._appReadyReject = () => {
        debug(`%s: attach cancelled`, this._name);
        this._state = INIT;
        delete this._appReadyResolve;
        delete this._appReadyReject;
        reject();
      }
    });

    if (this._onAttach) this._onAttach();

    return this._appReadyPromise;
  }

  getIFrame() {
    return this._iframe;
  }

  detach() {
    if (this._state !== INIT) {
      if (this._state !== READY) {
        delete this._appReadyReject();
      }
      if (this._iframe) {
        this._div.removeChild(this._iframe);
        delete this._iframe;
        delete this._div;
      }
      this._state = INIT;
      if (this._onDetach) this._onDetach();
    }
  }

  /**
   * Обрабатывает payload сообщений полученных из соотвествующего iframe и признаком что это сообщение mixed-app-manager'а.
   *
   * @param payload
   * @private
   */
  _processMessage(payload) {
    debug(`%s: _processMessage(%o)`, this._name, payload);
    switch (payload.type) {
      case 'loaded': {
        this._state = LOADED;
        this._mainApp._storage._sendStateToApp(this);
        break;
      }
      case 'ready': {
        if (this._pushOnReadyURL) {
          const url = this._toChildUrl(this._pushOnReadyURL);
          if (url) this.sendMessage({
            type: 'push-url',
            url,
          });
          delete this._pushOnReadyURL;
        }
        this._appReadyResolve();
        break;
      }
      case 'push-url': {
        this._mainApp.pushUrl(this._toParentUrl(payload.url));
        break;
      }
      case 'replace-state': {
        this._mainApp.replaceUrl(this._toParentUrl(payload.url));
        break;
      }
      case 'history-go': {
        this._mainApp.historyGo(payload.n);
        break;
      }
      case 'new-tab': {
        this._mainApp.newTab(payload.url);
        break;
      }
      case 'storage': {
        this._mainApp._storage.update(payload.value);
        break;
      }
      default:
        throw new Error(`Unexpected message: ${JSON.stringify(payload)}`);
    }
  };

  /**
   * Отправляет сообщение в окно вложенного приложения.
   *
   * @param msg - сообщение
   */
  sendMessage(msg) {
    debug(`%s: sendMessage(%o)`, this._name, msg);
    if (this._iframe) {
      this._iframe.contentWindow.postMessage({
        source: 'mixed-app-manager',
        payload: msg,
      }, '*');
    } else {
      debug(`%s: no iframe`, this._name);
    }
  }
}

Object.defineProperties(
  MainAppSubApp.prototype,
  {
    name: {
      get() {
        return this._name;
      }
    },
    started: {
      get() {
        return this._appReadyPromise;
      }
    },
    _state: {
      set(v) {
        debug(`%s: _state = %s`, this.name, v);
        this.__state = v;
      },
      get() {
        return this.__state;
      },
    },
  }
);

function identity(v) {
  return v;
}

export default MainAppSubApp;
