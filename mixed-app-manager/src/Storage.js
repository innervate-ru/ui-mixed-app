import {invalidArgument, missingArgument, unexpectedArgument} from "./_arguments";
import {READY} from "./_states";
import MainApp from "./MainApp";

const debug = require('debug')('mam'); // zork: Полный путь до debug, убирает ошибку в IE 11

class Storage {

  constructor({app, doSafeToStorage, storage}) {
    this._state = Object.create(null);
    this._listeners = [];
    this._app = app;
    this._storage = app.isMain && storage ? window[storage] : null;
    this._doSafeToStorage = doSafeToStorage;
    if (this._storage) {
      Object.entries(this._storage).forEach(([key, value]) => this._state[key] = value);
    }
  }

  update(state = missingArgument('state')) {
    if (!(typeof state === 'object' && state != null && Object.values(state).every(v => v === undefined || typeof v === 'string'))) invalidArgument('state', state);
    debug(`%s: update(%o)`, this._app._name, state);
    if (this._app._isMain) {
      this._update(state);
    } else {
      this._app.sendToParent({
        type: 'storage',
        value: state,
      });
    }
  }

  /**
   * Обновления полученные через сообщение - только для распостранения обновлений сверху вниз.
   * @private
   */
  _update(state = missingArgument('state')) {
    const prevState = this._state;
    const newState = this._state = Object.create(null);
    Object.entries(prevState).forEach(([key, value]) => {
      newState[key] = value;
    });
    Object.entries(state).forEach(([key, value]) => {
      if (value === undefined) {
        delete newState[key];
      } else {
        newState[key] = value;
      }
    });
    for (let i = this._listeners.length; i-- > 0;) { // в обратной последовательности, чтоб listener мог себя безопастно в процессе удалить
      const listener = this._listeners[i];
      listener(newState, prevState);
    }
    if (this._app.isMain) {
      if (this._storage) {
        const doSafeToStorage = this._doSafeToStorage;
        Object.entries(state).forEach(([key, value]) => {
          if (doSafeToStorage && !doSafeToStorage(key)) return;
          if (value === undefined) {
            this._storage.removeItem(key);
          } else {
            this._storage.setItem(key, value);
          }
        });
      }
      this._app.sendToAllLoaded({
        type: 'storage',
        value: state,
      });
    }
  }

  _sendStateToApp(subApp) {
    subApp.sendMessage({
      type: 'storage',
      value: this._state,
    })
  }

  setItem(key, value) {
    if (!(typeof key === 'string')) invalidArgument('key', key);
    // if (!(typeof value === 'string')) invalidArgument('value', value);
    debug(`%s: setItem(%s, %s)`, this._app._name, key, value);
    this.update({
      [key]: `${value}`,
    });
  }

  getItem(key) {
    if (!(typeof key === 'string')) invalidArgument('key', key);
    debug(`%s: getItem(%s)`, this._app._name, key);
    return this._state[key] || null;
  }

  /**
   * @param listener
   * @returns {Function} - метод, чтоб отписаться от получения обновлений состояния
   */
  onUpdate(listener, noTimeout) {
    if (!(typeof listener === 'function')) invalidArgument('listener', listener);
    debug(`%s: onUpdate()`, this._app._name);
    this._listeners.push(listener);
    if (noTimeout) { // во app.vue created() нужно чтобы listener сработал синронно сразу - иначе срабатыват логика до момента когда удается получить данные о пользователе
      listener(this._state, this._state);
    } else {
      setTimeout(() => listener(this._state, this._state), 0);
    }
    return () => {
      const index = this._listeners.findIndex((v) => v === listener);
      if (index >= 0) {
        this._listeners.splice(index, 1);
      }
    };
  }
}

Object.defineProperties(
  Storage.prototype,
  {
    state: {
      get() {
        return this._state;
      }
    },
  }
);

export default Storage;
