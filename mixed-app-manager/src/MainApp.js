import URL from 'url'
import queryString from 'query-string'
import {invalidArgument, unexpectedArgument} from "./_arguments";
import MainAppSubApp from "./MainAppSubApp";
import Storage from "./Storage";
import {INIT, READY} from "./_states";
import {isMain, parsedURL} from "./index";

const debug = require('debug')('mam'); // zork: Полный путь до debug, убирает ошибку в IE 11

class MainApp {

  constructor({isMain, name, parentHost}) {
    this._subApps = Object.create(null);
    this._state = INIT;
    this._isMain = isMain;
    this._name = name;
    this._storage = null;
    this._appReadyPromise = new Promise((resolve, reject) => {
      this._appReadyResolve = () => {
        debug(`%s: state: ready`, this._name);
        this._state = READY;
        delete this._appReadyResolve;
        resolve(this);
      }
    });
    this._parentHost = parentHost;
    this._listeners = [];
  }

  init(args) { // zork: Почему то транспайлер глючит, и оставляет этот async в ES5 коде
    debug(`%s: init(%o)`, this._name, args);
    let storage, subApps, doSafeToStorage, toParentUrl;
    if (args !== undefined) {
      if (!(typeof args === 'object' && args != null)) invalidArgument('args', args);
      Object.entries(args).forEach(([key, value]) => {
        switch (key) {
          case 'storage': {
            if (!(value === undefined || value === 'localStorage' || value === 'sessionStorage')) invalidArgument('storage', value);
            storage = value;
            break;
          }
          case 'subApps': {
            if (!(!value || (Array.isArray(value) && value.every(v => typeof v === 'object' && v !== null)))) invalidArgument('subApps', value)
            subApps = value;
            break;
          }
          case 'doSafeToStorage': {
            if (!(!value || typeof value === 'function')) invalidArgument('doSafeToStorage', value);
            doSafeToStorage = value;
            break;
          }
          case 'toParentUrl': {
            if (!(!value || typeof value === 'function')) invalidArgument('toParentUrl', value);
            toParentUrl = value;
            break;
          }
          default:
            unexpectedArgument(key, value);
        }
      });
    }
    this._storage = new Storage({app: this, doSafeToStorage, storage});
    this._toParentUrl = (isMain || !toParentUrl) ? (url) => url : (toParentUrl.bind(this));
    /* zork: Это оказалось лишней логикой
        if (this._parentHost) {
          const prevToParentUrl = this._toParentUrl;
          const {protocol, hostname, port} = URL.parse(this._parentHost);
          this._toParentUrl = function (url) {
            return URL.format({
              ...URL.parse(prevToParentUrl(url)),
              protocol,
              hostname,
              port,
            });
          }
        }
    */

    if (subApps) {
      this.addSubApp.apply(this, subApps);
    }
    if (this._isMain) {
      this._appReadyResolve();
    }
  }

  _pushUrl(url) {
    debug(`%s: _pushUrl(%s); this._hook.pushUrl: %o`, this._name, url, this._hook && this._hook.pushUrl);
    (this._hook && this._hook.pushUrl) ? this._hook.pushUrl(url) : window.history.pushState(null, '', url);
  };

  _replaceUrl(url) {
    debug(`%s: _replaceUrl(%s)`, this._name, url);
    (this._hook && this._hook.replaceUrl) ? this._hook.replaceUrl(url) : window.history.replaceState(null, '', url);
  };

  _historyGo(n) {
    (this._hook && this._hook.historyGo) ? this._hook.historyGo(n) : window.history.go(n);
  };

  _newTab(url) {
    (this._hook && this._hook.newTab) ? this._hook.newTab(url) : window.open(url, '_blank');
  };

  setHook(hook) {
    this._hook = hook;
  }

  _url(url) {
    if (typeof url === 'object' && url !== null) {
      if (url.query) {
        const parsedUrl = URL.parse(url.path);
        parsedURL.search = `?${queryString.stringify(url.query)}`;
        const newUrl = {
          ...parsedUrl,
          search: queryString.stringify(url.query),
        };
        return URL.format(newUrl);
      }
      return url.path;
    }
    return url;
  }

  toParentUrl(url) {
    return this._toParentUrl(this._url(url));
  }

  /**
   * Добавляет приложение.
   *
   * Приложение описывается структурой:
   * {
   * name - имя приложения,
   * baseUrl - базовый адрес приложения, без слеша на конце (например: http://localhost:8090),
   * blankPage? - пустая страница приложения (используется при переходах, чтоб не было видно предыдущего состояния)
   * toParentUrl? - функция преобразование адресов из вложенного приложения в главное приложение,
   * toParentUrl? - функция преобразование адресов из главного приложения во вложенное прилжение
   * onAttach? - вызывается в момент создания iframe
   * onDetach? - вызывается в момент удаления iframe
   * }
   *
   * Чтобы добавить несколько приложений, надо передать несколько аргументов.
   */
  addSubApp(...args) {
    debug(`%s: addSubApp(%o)`, this._name, args);
    args.forEach((v) => {
      const subapp = new MainAppSubApp(this, v);
      if (Object.prototype.hasOwnProperty.call(this._subApps, subapp._shortName)) throw new Error(`Duplicated app name: ${subapp.name}`);
      this._subApps[subapp._shortName] = subapp;
    });
  }

  _processMessage(event) {
    const payload = event.data.payload;

    /* zork: same logic in the SubApp class
        if (!(typeof event === 'object' && event !== null && event.data.source === 'mixed-app-manager')) return;
        debug(`%s: _processMessage(%o)`, this._name, event);
    */
    const subapp = Object.values(this._subApps).find(app => app._iframe && event.source === app._iframe.contentWindow);
    if (!subapp) {
      throw new Error(`Unexpected event.origin`);
    }
    subapp._processMessage(event.data.payload);
  };

  /* async */
  getApp(name, args) {
    debug(`%s: getApp(%s, %o)`, this._name, name, args)
    if (!this._subApps[name]) throw new Error(`Missing app with name '${name}'`)
    return this._subApps[name];
  }

  sendToAllLoaded(msg) {
    Object.values(this._subApps).forEach((app) => {
      if (app._state !== INIT) app.sendMessage(msg);
    })
  }

  broadcast(msg) {
    for (let i = this._listeners.length; i-- > 0;) { // в обратной последовательности, чтоб listener мог себя безопастно в процессе удалить
      const listener = this._listeners[i];
      listener(msg);
    }
    this.sendToAllLoaded({msg, type: 'broadcast'});
  }

  /**
   * @param listener
   * @returns {Function} - метод, чтоб отписаться от получения обновлений состояния
   */
  onMessage(listener) {
    if (!(typeof listener === 'function')) invalidArgument('listener', listener);
    debug(`%s: onUpdate()`, this._name);
    this._listeners.push(listener);
    return () => {
      const index = this._listeners.findIndex((v) => v === listener);
      if (index >= 0) {
        this._listeners.splice(index, 1);
      }
    };
  }
}

Object.defineProperties(
  MainApp.prototype,
  {
    isMain: {
      get() {
        return this._isMain;
      },
    },
    name: {
      get() {
        return this._name;
      },
    },
    storage: {
      get() {
        return this._storage;
      },
    },
    started: {
      get() {
        return this._appReadyPromise;
      },
    },
  },
);

export default MainApp;
