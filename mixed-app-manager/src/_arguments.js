export function missingArgument(name) {
  throw new Error(`Missing argument '${name}'`);
}

export function invalidArgument(name, value) {
  throw new Error(`Invalid argument '${name}' : ${JSON.stringify(value)}`);
}

export function unexpectedArgument(name, value) {
  throw new Error(`Unexpected argument '${name}' : ${JSON.stringify(value)}`);
}
