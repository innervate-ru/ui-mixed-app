export const INIT = 'init'; // initial
export const INITIALIZING  ='initializing'; // initialization step is completed; MainAppSubApp not using this step
export const LOADING = 'loading'; // MainAppSubApp - had started loading iframe; other types of apps skip it
export const LOADED = 'loaded'; // MainAppSubApp - iframe is loaded; other types of apps skip it
export const READY = 'ready'; // app is loaded and storage is initialized.  app is ready to work
