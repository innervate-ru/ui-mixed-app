import URL from 'url'
import queryString from 'query-string'
import SubApp from './SubApp'

const debug = require('debug')('mam'); // zork: Полный путь до debug, убирает ошибку в IE 11

export const parsedURL = URL.parse(window.location.href);
const query = queryString.parse(parsedURL.query);

export const isMain = !query._ma;
export const name = isMain ? 'main' : decodeURI(query._ma);
export const childUrl = query._maChildUrl;
const parentHost = query._maParentHost;

debug('%s: started (url: %s)', name, window.location.href);

if (!isMain) { // удаляем параметр _ma из URL, чтоб он не попал при переходах в верхнее приложение
  delete query._ma;
  delete query._maParentHost;
  delete query._maChildUrl;
  parsedURL.search = queryString.stringify(query);
  window.history.replaceState(undefined, '', URL.format(parsedURL));
}

const app = new SubApp({isMain, name, parentHost});

window.addEventListener('message', app._processMessage.bind(app));

if (!app.isMain) {
  debug(`%s: state: loaded`, app.name);
  app.sendToParent({type: 'loaded'});
}

export default app;
