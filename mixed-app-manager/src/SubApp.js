import {invalidArgument, missingArgument, unexpectedArgument} from "./_arguments";
import {READY} from "./_states";
import MainApp from './MainApp'

const debug = require('debug')('mam'); // zork: Полный путь до debug, убирает ошибку в IE 11

class SubApp extends MainApp {

  constructor(options) {
    super(options);
  }

  pushUrl(url) {
    if (!(typeof url === 'string')) invalidArgument('url', url);
    debug(`%s: pushUrl(%s)`, this._name, url);
    if (this._state !== READY) throw new Error(`Invalid state: ${this._state}`);
    if (this._isMain) {
      super._pushUrl(url);
    } else {
      this.sendToParent({
        type: 'push-url',
        url,
      });
    }
  };

  replaceUrl(url) {
    if (!(typeof url === 'string')) invalidArgument('url', url);
    debug(`%s: replaceUrl(%s)`, this._name, url);
    if (this._state !== READY) throw new Error(`Invalid state: ${this._state}`);
    if (this._isMain) {
      super._replaceUrl(url);
    } else {
      this.sendToParent({
        type: 'replace-state',
        url,
      });
    }
  };

  historyGo(n) {
    if (!(typeof n !== 'number' && n > 0)) invalidArgument('n', n);
    debug(`%s: historyGo(%d)`, this._name, n);
    if (this._state !== READY) throw new Error(`Invalid state: ${this._state}`);
    if (this._isMain) {
      super._historyGo(url);
    } else {
      this.sendToParent({
        type: 'history-go',
        n,
      })
    }
  };

  newTab(url) {
    if (!(typeof url === 'string')) invalidArgument('url', url);
    debug(`%s: newTab(%s)`, this._name, url);
    if (this._state !== READY) throw new Error(`Invalid state: ${this._state}`);
    if (this._isMain) {
      super._newTab(url);
    } else {
      this.sendToParent({
        type: 'new-tab',
        url,
      })
    }
  };

  sendToParent(msg) {
    debug(`%s: _sendToParent(%o)`, this._name, msg);
    window.parent.postMessage({
      source: 'mixed-app-manager',
      payload: msg,
    }, '*');
  };

  _processMessage(event) {
    if (!(typeof event === 'object' && event !== null && event.data.source === 'mixed-app-manager')) return;
    debug(`%s: _processMessage(%o)`, this._name, event);
    const payload = event.data.payload;
    if (payload.type === 'broadcast') {
      for (let i = this._listeners.length; i-- > 0;) { // в обратной последовательности, чтоб listener мог себя безопастно в процессе удалить
        const listener = this._listeners[i];
        listener(payload.msg);
      }
      this.sendToAllLoaded(payload.msg);
    } else if (this._isMain) {
      super._processMessage(event);
    } else {
      switch (payload.type) {
        case 'storage': {
          this._storage._update(payload.value);
          if (this._state !== READY) {
            this._appReadyResolve();
            delete this._appReadyResolve;
            this.sendToParent({type: 'ready'});
          }
          break;
        }
        case 'push-url': {
          super._pushUrl(payload.url);
          break;
        }
        case 'replace-url': {
          super._replaceUrl(payload.url);
          break;
        }
        case 'history-go': {
          super._historyGo(payload.n);
          break;
        }
        default:
          throw new Error(`Unexpected message: ${JSON.stringify(payload)}`);
      }
    }
  }
}

export default SubApp;
