import mixedApp from '../../../mixed-app-manager/src'

const debug = require('debug')('mam'); // zork: Полный путь до debug, убирает ошибку в IE 11

export default async ({Vue, app}) => {
  await mixedApp.init({
    storage: 'localStorage',
    subApps: [
      {
        name: 'subapp',
        baseUrl: 'http://localhost:8080',
        toParentUrl: require('../mixedApp/subApp/toParentUrl').default,
        toChildUrl: require('../mixedApp/subApp/toChildUrl').default,
      },
    ],
  });
  await mixedApp.started;
}
