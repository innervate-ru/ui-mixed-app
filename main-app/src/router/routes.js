const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'config', component: () => import('pages/Config.vue') },
      { path: 'subapp1a*', component: () => import('pages/SubApp1a.vue') },
      { path: 'subapp1b*', component: () => import('pages/SubApp1b.vue') },
      { path: 'subapp1b/*', component: () => import('pages/SubApp1b.vue') },
      { path: 'url1', component: () => import('pages/Url1.vue') },
      { path: 'url2', component: () => import('pages/Url2.vue') },
      { path: 'url3', component: () => import('pages/Url3.vue') },
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
