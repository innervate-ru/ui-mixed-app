import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'

import mixedApp from '../../../mixed-app-manager/src'

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  });

  mixedApp.setHook({
    pushUrl: (url) => { Router.push(url); },
    replaceUrl: (url) => { Router.replace(url); },
    historyGo: (n) => { Router.go(n); },
  });

  // const _push = Router.push;
  // Router.push = function() {
  //   console.info(27);
  //   _push.apply(Router, arguments);
  // }
  //
  // const _replace = Router.replace;
  // Router.replace = function() {
  //   console.info(33);
  //   _replace.apply(Router, arguments);
  // }
  //
  // const _go = Router.go;
  // Router.go = function() {
  //   console.info(39);
  //   _go.apply(Router, arguments);
  // }

  return Router
}
