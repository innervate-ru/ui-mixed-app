import URL from 'url'

const debug = require('debug')('mam'); // zork: Полный путь до debug, убирает ошибку в IE 11

export default function (url) {
  const parsedURL = URL.parse(url);

  const r = parsedURL.pathname.match(/\/subapp1b(\/.*)?/);
  parsedURL.pathname = `${r[1] || '/'}`;

  const resultURL = URL.format(parsedURL);
  debug(`%s: toChildUrl: %s -> %s`, this.name, url, resultURL);
  return resultURL;
}
